# Docker, Dockerfile, Docker-compose

Projet pour comprendre Docker et ses outils.

# Quelques commandes Linux
- ip addr : voir les ip
- cd : se déplacer dans les répertoires.
- ls : lister les répertoires et fichiers.
- ls -lsa : voir les fichier cachés
- pwd : afficher le répertoire où l'on se trouve exactement.
- cp : copier des fichiers ou des répertoires.
- mkdir : créer un dossier
- mv : renommer ou déplacer un fichier ou un dossier.
- ln : créer des liens durs et liens symboliques.
- rm : supprimer un fichier ou un dossier.
- rmdir : supprimer un dossier vide (plus sûre que rm).
- touch : créer un fichier.
- find, recherche pointue !
- locate, Recherche accélérée de fichiers ou répertoire.
- which, Chemin d'une commande déterminée.
- chmod : gérer la distribution des droits d'un fichier.
- chown : gérer la propriété d'un fichier.
- umask : création automatique des droits.
- Access Control List : Gestion des droits avancés (experts).
- cat : lire le contenu d'un fichier texte.
- checksum: Vérifier l'intégrité d'un fichier.

# Commande Docker

- docker run : Lancer un conteneur docker.
- -d pour detach pour que le conteneur reste allumé.
- -p pour definir le port d'utilisation.
- le dernier argument est l'image .

        ex: docker run -d -p 8080:80 nginx

Ouvrir le navigateur sur l'ip local ou de la machine virtuel ex: 192.168.1.14:8080
- docker ps: pour voir si conteneur actif.
- docker ps -a: pour voir les images presentes en local .
- docker stop ID_RETOURNÉ_LORS_DU_DOCKER_RUN : kill un conteneur docker.
- docker rm ID_RETOURNÉ_LORS_DU_DOCKER_RUN : supprimer le conteneur apres l'avoir kill.
- docker system prune : nettoyage du syteme
- docker pull : récupération d'une image depuis une registry

# Dockerfile

- FROM qui vous permet de définir l'image source ;
- RUN qui vous permet d’exécuter des commandes dans votre conteneur ;
- ADD qui vous permet d'ajouter des fichiers dans votre conteneur ;
- WORKDIR qui vous permet de définir votre répertoire de travail ;
- EXPOSE qui permet de définir les ports d'écoute par défaut ;
- VOLUME qui permet de définir les volumes utilisables ;
- CMD qui permet de définir la commande par défaut lors de l’exécution de vos conteneurs Docker.
- -t : donne le nom à l'image 

      ex: docker build -t nom_image .

# Docker-compose
- docker-compose up : démarrer la stack du docker-compose.
- docker-compose up -d : démarrer la stack du docker-compose en arriere plan.
- docker-compose ps : permet de voir le status de l'ensemble de la stack.
- docker-compose stop : arreter une stack docker-compose.
- docker-compose down : supprimer l'ensemble de la stack Docker Compose, detruit l'ensemble des ressources.
- docker-compose config : valider la synthax du docker-compose


# Qu'est ce qu'un conteneur ?

Docker est une alternative à la machine virtuelle, Docker est une virtualisation légére quand la machine virtuelle est une virtualisation lourde. Les conteneur apporte une isolation importante des processus systemes et à la differences d'une machine virtuelle les ressources CPU, RAM et disque sont totalement partagées avec l'ensemble du systeme. Les conteneurs partagent entre eux le kernel Linux ; ainsi, il n'est pas possible de faire fonctionner un système Windows ou BSD dans celui-ci. 
Un conteneur démarre beaucoup plus rapidement qu'une VM et ameliore le cycle de deploiement. Dans la vision Docker, un conteneur ne doit faire tourner qu'un seul processus. Ainsi, dans le cas d'une stack LAMP (Linux, Apache, MySQL, PHP), nous devons créer 3 conteneurs différents, un pour Apache, un pour MySQL et un dernier pour PHP. Alors que dans un conteneur LXC ou OpenVZ, nous aurions fait tourner l'ensemble des 3 services dans un seul et unique conteneur.

Stateless vs Stateful

Dans le monde de Docker, vous allez souvent entendre parler de stateless et stateful, deux catégories de conteneurs, et vous devez savoir à quoi correspond chaque catégorie.

- stateful pour une application qui stocke un etat type base de données
- stateless pour une application qui ne stocke pas l'etat.

L'immutabilité d'un conteneur est aussi importante. Un conteneur ne doit pas stocker de données qui doivent être pérennes, car il les perdra (à moins que vous les ayez pérennisées). Mais si vous souhaitez en local mettre une base de données dans un conteneur Docker, vous devez créer un volume pour que celui-ci puisse stocker les données de façon pérenne.

# installation de docker

1 - Configurer le référentiel

     sudo apt-get update
     sudo apt-get install \
        ca-certificates \
        curl \
        gnupg \
        lsb-release

2 - Ajoutez la clé GPG officielle de Docker :

        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

3 - Installer le moteur Docker

        sudo apt-get update
        sudo apt-get install docker-ce docker-ce-cli containerd.io

4 - Vérifiez que Docker Engine est correctement installé en exécutant l' hello-world image.

        sudo docker run hello-world


# Dockerfile

Dockerfilesert à creer sa propre image docker.

1 - Faire un clone du projet de test

                sudo git clone https://github.com/OpenClassrooms-Student-Center/ghost-cms.git

2 - Creer un fichier Dockerfile à la racine du projet 

        sudo touch Dockerfile

3 - Editer le fichier

        nano Dockerfile

Si nano n'est pas reconnu

                sudo apt-get update
                sudo apt-get install nano

4 - entrer les informations suivantes

                FROM debian:9

                RUN apt-get update -yq \
                && apt-get install curl gnupg -yq \
                && curl -sL https://deb.nodesource.com/setup_10.x | bash \
                && apt-get install nodejs -yq \
                && apt-get clean -y

                ADD . /app/
                WORKDIR /app
                RUN npm install

                EXPOSE 2368
                VOLUME /app/logs

                CMD npm run start

Fermer et sauvegarder le Dockerfile

Voir ci-dessus pour les explications des commandes

5 - Donner un nom au conteneur

                docker build -t nom_image

6 - Run le conteneur

                sudo docker run -d -p 2368:2368 nom_image

7 - Accéder au contenu

          http://192.168.1.14:2368/  ou 127.0.0.1:2368 machine local


# Docker-compose

Docker Compose va vous permettre d'orchestrer vos conteneurs, et ainsi de simplifier vos déploiements sur de multiples environnements. Docker Compose est un outil écrit en Python qui permet de décrire, dans un fichier YAML, plusieurs conteneurs comme un ensemble de services. 

Exemple avec un conteneur MySql et un conteneur WordPress

1 - Creer un fichier docker-compose.yml à la racine du projet

                sudo touch docker-compose.yml


2 - Entrer dans le fichier docker-compose.yml

                sudo nano docker-compose.yml

3 - Definir la version du docker compose

                version: '3'

4 -  Déclaration du service MySql

                services:
                        db:
                        image: mysql:5.7
                        volumes:
                           - db_data:/var/lib/mysql
                        restart: always
                        environment:
                           MYSQL_ROOT_PASSWORD: somewordpress
                           MYSQL_DATABASE: wordpress
                           MYSQL_USER: wordpress
                           MYSQL_PASSWORD: wordpress

- image : permet de definir l'image Docker a utiliser ou "build" en specifiant le chemin pour un Dockerfile.
- volumes : les conteneurs Dockers ne sont pas faits pour faire focntionner des services stateful, les bases de données sont des services stateful du coup volume permet de stocker l'ensemble du contenu du dossier /var/lib/mysql ans un disque persistant. Et donc, de pouvoir garder les données en local sur notre host. db_data est un volume créé par Docker directement, qui permet d'écrire les données sur le disque hôte sans spécifier l'emplacement exact. Vous auriez pu aussi faire un /data/mysql:/var/lib/mysql qui serait aussi fonctionnel.
- restart: Les conteneurs étant par définition monoprocessus, si celui-ci rencontre une erreur fatale, il peut être amené à s'arrêter. Dans notre cas, si le serveur MySQL s'arrête, celui-ci redémarrera automatiquement grâce à l'argument restart: always.
- environnement : L'image MySQL fournie dispose de plusieurs variables d'environnement que vous pouvez utiliser ; dans votre cas, nous allons donner au conteneur les valeurs des différents mots de passe et utilisateurs qui doivent exister sur cette base. Quand vous souhaitez donner des variables d'environnement à un conteneur, vous devez utiliser l'argument environment, comme nous avons utilisé dans le fichier docker-compose.yml ci-dessus.

5 - Déclaration du service Wordpress

                services:
                  wordpress:
                   depends_on:
                      - db
                image: wordpress:latest
                ports:
                  - "8000:80"
                restart: always
                environment:
                   WORDPRESS_DB_HOST: db:3306
                   WORDPRESS_DB_USER: wordpress
                   WORDPRESS_DB_PASSWORD: wordpress
                   WORDPRESS_DB_NAME: wordpress

- depends_on : permet de créer une dépendance entre deux conteneurs. Ainsi, Docker démarrera le service db avant de démarrer le service Wordpress. Ce qui est un comportement souhaitable, car Wordpress dépend de la base de données pour fonctionner correctement.

- ports : permet de dire à Docker Compose qu'on veut exposer un port de notre machine hôte vers notre conteneur, et ainsi le rendre accessible depuis l'extérieur.

## docker-compose final

                version: '3'
                services:
                  db:
                    image: mysql:5.7
                volumes:
                    - db_data:/var/lib/mysql
                restart: always
                environment:
                     MYSQL_ROOT_PASSWORD: somewordpress
                     MYSQL_DATABASE: wordpress
                     MYSQL_USER: wordpress
                     MYSQL_PASSWORD: wordpress
                
                wordpress:
                depends_on:
                  - db
                image: wordpress:latest
                ports:
                  - "8000:80"
                restart: always
                environment:
                  WORDPRESS_DB_HOST: db:3306
                  WORDPRESS_DB_USER: wordpress
                  WORDPRESS_DB_PASSWORD: wordpress
                  WORDPRESS_DB_NAME: wordpress

                volumes:
                db_data: {}

## docker-compose final (with Dockerfile)

        version: '3'
            services:
              db:
                image: mysql:5.7
                volumes:
                  - db_data:/var/lib/mysql
                restart: always
                environment:
                  MYSQL_ROOT_PASSWORD: somewordpress
                  MYSQL_DATABASE: wordpress
                  MYSQL_USER: wordpress
                  MYSQL_PASSWORD: wordpress

              ghost:
                depends_on:
                  - db
                build:
                  context: .
                  dockerfile: ./Dockerfile
                ports:
                  - "2368:2368"
                restart: always

            volumes:
              db_data: {}

6 - Lancez la stack Docker Compose

                docker-compose up -d

6 - Voir le resultat

                http://127.0.0.1:8000 localhost ou http://192.168.1.14:8000 VM

